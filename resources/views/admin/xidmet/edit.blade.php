@extends('layouts.static')
@section('content')
<!-- Horizontal Layout -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>
    Xidmətlər
        </h2>
      </div>
    </div>
  </div>
</div>

<div class="row clearfix" style="padding-bottom:50px;">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <form class="form-horizontal" action="{{ route('services.update', $xidmet->id) }}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}
      <input type="hidden" name="_method" value="PATCH">
      <div class="tab-content" style="margin-top:50px;">
        <div class="col-md-12 clearfix" >
            <div class="col-md-6" style="padding-top: 25px;">
             <p>
               <b>Xidmət adı</b>
             </p>
             <input type="text"  class="form-control" name="xidmet_name"  required value="{{$xidmet->xidmet_name}}">
           </div>
           <div class="col-md-6" style="padding-top: 25px;">
            <p>
              <b>Xidmətə aid icon</b>
            </p>
            <label for="img" style="width:120px;height:120px;"><img src="/uploads/{{$xidmet->xidmet_icon}}"  class="img-responsive"/></label>
           <input type="file" class="form-control" name="xidmet_icon"  id="img">
          </div>
           <div class="col-md-12" style="padding-top: 25px;">
            <p>
              <b>Xidmətə mətni</b>
            </p>
           <textarea name="xidmet_text" rows="8" cols="80" id="article-ckeditor">{{$xidmet->xidmet_text}}</textarea>
          </div>


     </div>
      <div class="col-md-12" style="padding-top: 25px;">
        <div class="col-md-4">
            <button type="submit" name="button" class="btn btn-success ">Yarat</button>
        </div>
      </div>
    </form>
  </div>
</div>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'article-ckeditor' );
        CKEDITOR.replace( 'article-ckeditor1' );
    </script>
@endsection
