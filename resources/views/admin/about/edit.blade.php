@extends('layouts.static')
@section('content')
<!-- Horizontal Layout -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>

        </h2>
      </div>
    </div>
  </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

    <div>
      <form class="form-horizontal" action="{{ route('about.update', $about->id) }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
         <input type="hidden" name="_method" value="PATCH">

         <div class="tab-content" style="margin-top:50px;">
              <div class="col-md-12 clearfix" style="padding:0 50px;">
                <div class="tab-content" style="margin-top:50px;">
                     <div class="col-md-12 clearfix" style="padding:0 50px;">


                      <div class="col-md-12" style="padding-top: 25px;">
                       <p>
                         <b>Haqqmızda </b>
                       </p>
                       <textarea   class="form-control" name="about"  required id='about'>{{$about->about}}</textarea>
                     </div>
                     <div class="col-md-12" style="padding-top: 25px;">

                           <button type="submit" name="button" class="btn btn-success ">Yarat</button>

                     </div>
                   </div>

              </form>
            </div>
          </div>
        </div>
              <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
              <script>
                  CKEDITOR.replace( 'about' );
              </script>
          <!-- #END# Horizontal Layout -->

@endsection
