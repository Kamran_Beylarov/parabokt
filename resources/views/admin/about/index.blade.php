@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            @if(Session::has('mesaj'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                {{ Session::get('mesaj') }}
            </div>
            @endif
            <div class="header">
                <h2>
                  @if(!isset($haqq->about))
                <a href="{{ route('about.create') }}"><button type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">add</i>
                </button></a>
                @endif
            </div>
            <div class="body table-responsive">
              <div class="col-md-12" style="padding-top: 25px;">
               <p>
                 <b>Haqqmızda </b>
               </p>
               <textarea   class="form-control" name="about"   id='about' readonly>@if(isset($haqq->about)){{$haqq->about}}@endif</textarea>
             </div>
             <div class="col-md-12">

              {{--  <form action="{{ route('about.destroy', $haqq->id) }}" method="post" style="display: initial;">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" value="Sil" class="btn btn-danger">
                </form>--}}
                @if(isset($haqq->about))
                <a href="{{ route('about.edit', $haqq->id) }}" class="btn-success btn">Redaktə et</a>
                @endif
              </div>

            </div>
        </div>
    </div>
</div>
<!-- #END# Hover Rows -->
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'about' );
</script>
@endsection
