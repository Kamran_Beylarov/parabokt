@extends('layouts.static')
@section('content')

<!-- Horizontal Layout -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>
        Haqqmızda
        </h2>
      </div>
    </div>
  </div>
</div>

<div class="row clearfix" style="padding-bottom:50px;">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <form class="form-horizontal" action="{{ route('about.store') }}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}

      <div class="tab-content" style="margin-top:50px;">
           <div class="col-md-12 clearfix" style="padding:0 50px;">


            <div class="col-md-12" style="padding-top: 25px;">
             <p>
               <b>Haqqmızda </b>
             </p>
             <textarea   class="form-control" name="about"  required id='about'></textarea>
           </div>
           <div class="col-md-12" style="padding-top: 25px;">

                 <button type="submit" name="button" class="btn btn-success ">Yarat</button>

           </div>
         </div>

    </form>
  </div>
</div>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'about' );
    </script>
<!-- #END# Horizontal Layout -->

@endsection
