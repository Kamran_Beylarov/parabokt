@extends('layouts.static')
@section('content')
<!-- Horizontal Layout -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>

        </h2>
      </div>
    </div>
  </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

    <div>
      <form class="form-horizontal" action="{{ route('filiallar.update', $filial->id) }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
         <input type="hidden" name="_method" value="PATCH">

         <div class="tab-content" style="margin-top:50px;">
              <div class="col-md-12 clearfix" style="padding:0 50px;">
                  <div class="col-md-12" style="padding-top: 25px;">
                   <p>
                     <b>Filial adı</b>
                   </p>
                   <input type="text"  class="form-control" name="filial_name" value="{{$filial->filial_name}}" required>
                 </div>

                 <div class="col-md-6" style="padding-top: 25px;">
                  <p>
                    <b>Filial Kordinatı (x) </b>
                  </p>
                  <input type="number"  class="form-control" name="filial_cordinat_x" value="{{$filial->filial_cordinat_x}}" required step="0.00000000000000000001">
                </div>
                <div class="col-md-6" style="padding-top: 25px;">
                 <p>
                   <b>Filial Kordinatı (y) </b>
                 </p>
                 <input type="number"  class="form-control" name="filial_cordinat_y" value="{{$filial->filial_cordinat_y}}" required step="0.00000000000000000001">
               </div>
               <div class="col-md-6" style="padding-top: 25px;">
                <p>
                  <b>Filialın Ünvanı </b>
                </p>
                <input type="text"  class="form-control" name="filial_address" value="{{$filial->filial_address}}" required >
              </div>
              <div class="col-md-6" style="padding-top: 25px;">
               <p>
                 <b>Əlaqə Nömrəsi </b>
               </p>
               <input type="text"  class="form-control" name="filial_number" value="{{$filial->filial_number}}" required >
             </div>
              <div class="col-md-12" style="padding-top: 25px;">

                    <button type="submit" name="button" class="btn btn-success ">Yarat</button>

              </div>
            </div>

       </form>
     </div>
      </form>
  </div>
</div>

@endsection
