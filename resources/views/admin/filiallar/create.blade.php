@extends('layouts.static')
@section('content')
<!-- Horizontal Layout -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>
        Filial əlavə et
        </h2>
      </div>
    </div>
  </div>
</div>

<div class="row clearfix" style="padding-bottom:50px;">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <form class="form-horizontal" action="{{ route('filiallar.store') }}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}

      <div class="tab-content" style="margin-top:50px;">
           <div class="col-md-12 clearfix" style="padding:0 50px;">
               <div class="col-md-12" style="padding-top: 25px;">
                <p>
                  <b>Filial adı</b>
                </p>
                <input type="text"  class="form-control" name="filial_name" placeholder="Filial adını daxil edin" required>
              </div>

              <div class="col-md-6" style="padding-top: 25px;">
               <p>
                 <b>Filial Kordinatı (x) </b>
               </p>
               <input type="number"  class="form-control" name="filial_cordinat_x" placeholder="Filial Kordinatını (x) qeyd edin" required step="0.00000000000001">
             </div>
             <div class="col-md-6" style="padding-top: 25px;">
              <p>
                <b>Filial Kordinatı (y) </b>
              </p>
              <input type="number"  class="form-control" name="filial_cordinat_y" placeholder="Filial Kordinatını (y) qeyd edin" required step="0.00000000000001">
            </div>
            <div class="col-md-6" style="padding-top: 25px;">
             <p>
               <b>Filialın Ünvanı </b>
             </p>
             <input type="text"  class="form-control" name="filial_address" placeholder="Filialın Ünvanını qeyd edin" required >
           </div>
           <div class="col-md-6" style="padding-top: 25px;">
            <p>
              <b>Əlaqə Nömrəsi </b>
            </p>
            <input type="text"  class="form-control" name="filial_number" placeholder="Əlaqə Nömrəsini qeyd edin" required >
          </div>
            <!-- <div class="col-md-12" style="padding-top: 25px;">
             <p>
               <b>Filial Haqqında Qısa Məlumat </b>
             </p>
             <textarea   class="form-control" name="filial_about"  required></textarea>
           </div> -->
           <div class="col-md-12" style="padding-top: 25px;">

                 <button type="submit" name="button" class="btn btn-success ">Yarat</button>

           </div>
         </div>

    </form>
  </div>
</div>
<!-- #END# Horizontal Layout -->

@endsection
