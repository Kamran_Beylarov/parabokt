@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            @if(Session::has('mesaj'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                {{ Session::get('mesaj') }}
            </div>
            @endif
            <div class="header">
                <h2>
                <a href="{{ route('filiallar.create') }}"><button type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">add</i>
                </button></a>
            </div>
            <div class="body table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                          <th>ID</th>
                        <th>Filial Adı</th>
                        <th>Filial kordinatı (x)</th>
                        <th>Filial kordinatı (y)</th>
                        <th>Filialın Ünvanı</th>
                        <th>Filialın Əlaqə Nömrəsi (y)</th>
                            <th><span class="text-danger">Sil</span> / <span class="text-success">Redaktə et</span></th>
                        </tr>
                    </thead>
                    <tbody>
                       @foreach($filiallar as $filial)
                        <tr>
                            <td>{{ $filial->id }}</td>
                            <td>{{ $filial->filial_name }}</td>
                            <td>{{ $filial->filial_cordinat_x }}</td>
                            <td>{{ $filial->filial_cordinat_y }}</td>
                            <td>{{ $filial->filial_address }}</td>
                            <td>{{ $filial->filial_number }}</td>
                            <td>
                                <form action="{{ route('filiallar.destroy', $filial->id) }}" method="post" style="display: initial;">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="submit" value="Sil" class="btn btn-danger">
                                </form>
                                <a href="{{ route('filiallar.edit', $filial->id) }}" class="btn-success btn">Redaktə et</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- #END# Hover Rows -->

@endsection
