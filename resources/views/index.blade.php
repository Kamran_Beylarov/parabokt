<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <!-- <meta name="google-site-verification" content="08XPRc7Iu0F_lC1TZ6eLEHknK99munmimE29DoCCOOw" /> -->
        <meta name="google-site-verification" content="dwiPYIP0NPvMu5OKxmeFxS42Uv2sPGyjB0dPRYtZcT0" />
        <title>Parabokt</title>

        <link rel="apple-touch-icon" sizes="57x57" href="{{ url("front/fav/apple-icon-57x57.png") }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ url("front/fav/apple-icon-60x60.png") }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ url("front/fav/apple-icon-72x72.png") }}">
        <link rel="apple-touch-icon" sizes="76x7" href="{{ url("front/fav/apple-icon-76x76.png") }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ url("front/fav/apple-icon-114x114.png") }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ url("front/fav/apple-icon-120x120.png") }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ url("front/fav/apple-icon-144x144.png") }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ url("front/fav/apple-icon-152x152.png") }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ url("front/fav/apple-icon-180x180.png") }}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{ url("front/fav/android-icon-192x192.png") }}">
        <link rel="icon" type="image/png" sizes="32x32"  href="{{ url("front/fav/favicon-32x32.png") }}">
        <link rel="icon" type="image/png" sizes="96x96"  href="{{ url("front/fav/favicon-96x96.png") }}">
        <link rel="icon" type="image/png" sizes="16x16"  href="{{ url("front/fav/favicon-16x16.png") }}">
        <link rel="manifest" href="{{ url("front/fav/manifest.json") }}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ url("front/fav/ms-icon-144x144.png") }}">
        <meta name="theme-color" content="#ffffff">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="robots" content="index, follow">
        <meta name="keywords" content="Para , Kredit, Parabokt, Lombard, Bank Olmayan Kredit Təşkilatı ,Partnyor Krediti, Lombard Xidməti, Xİdmet, Pul">
        <meta name="description" content="Parabokt">
        <meta name="author" content="Qayğılarınızı azaltmaq, arzularınızı reallaşdırmaq üçün PARABOKT var.">

        <!-- Facebook share header -->
        <meta property="og:title" content="Parabokt">
        <meta property="og:description" content="Qayğılarınızı azaltmaq, arzularınızı reallaşdırmaq üçün PARABOKT var.">
        <meta property="og:image" content="{{ url("front/img/share.jpg") }}">
        <meta property="og:url" content="domain">

        <!--Twitter header-->
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:image" content="{{ url("front/img/share.jpg") }}">
        <meta name="twitter:image:alt" content="">
        <meta name="twitter:creator" content="@endorphin"/>
        <meta name="twitter:site" content="@parabokt">

    <!-- Fonts -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Plugins -->
    <link rel="stylesheet" href="{{url('front/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{url('front/css/icofont.css')}}" />
    <link rel="stylesheet" href="{{url('front/css/animate.css')}}" />
    <link rel="stylesheet" href="{{url('front/css/owl.carousel.css')}}" />
    <link rel="stylesheet" href="{{url('front/css/ekko-lightbox.css')}}" />
    <link rel="stylesheet" href="{{url('front/sitefonts/stylesheet.css')}}" />
    <link rel="stylesheet" href="{{url('front/css/style.css?v1.2')}}" />
    <link rel="stylesheet" href="{{url('front/css/responsive.css?v1.0')}}" />

        <!-- Core Style Css -->
  <style media="screen">
      @media only screen and (max-width: 767px)
        {
            .link{
              margin-top: 15px;
            }
        }
  </style>

    <script src="{{url('front/js/jquery-min.js')}}"></script>
    <!--[if gte IE 9]>
  <style type="text/css">
    .gradientback {
       filter: none;
    }
  </style>
<![endif]-->

    </head>

    <body>
      <div class="kredit">
        <div class="kredit_main">
          <div class="kredit_form_box">
            <form class="clearfix row" action="/reuqest_kredo" method="post">
              {{ csrf_field() }}

              <img src="{{url('/front/img/close_pop.svg')}}" alt="" class="close_credit">
              <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <h3 class="text-center">Online kredit müraciəti</h3>
              </div>
              <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <p><b>Ad, Soyad, Ata adı* </b></p>
                <input type="text" name="name_surname" value=""   required class="form-control">
              </div>
              <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                <p><b>Şəxsiyyət vəsiqəsinin seriyası* </b></p>
                <input type="text" name="vesiqe_nom" value=""  required class="form-control">
              </div>
              <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                <p><b>Doğum tarixi* </b></p>
                <input type="date" name="dogum_tarixi" value=""   required class="form-control">
              </div>
              <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                <p><b>Qeydiyyat ünvanı* </b></p>
                <input type="text" name="unvan" value=""   required class="form-control">
              </div>
              <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                <p><b>Yaşayış üvanı* </b></p>
                <input type="text" name="yashayish_unvan" value=""   required class="form-control">
              </div>
              <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                <p><b>Şəhər nömrəsi* </b></p>
                <input type="number" name="seher_nom" value=""   required class="form-control">
              </div>
              <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                <p><b>Mobil nömrə* </b></p>
                <input type="number" name="mob_nom" value=""   required class="form-control">
              </div>
              <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                <p><b>Qızıl-zinət əşyalarının qramı (təxmini)* </b></p>
                <input type="number" name="qizil_qram" value=""   required class="form-control">
              </div>
              <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                <p><b>Qızılın əyyarı* </b></p>
                <input type="number" name="qizil_eyyar" value=""  required class="form-control">
              </div>
              <div class="col-md-12 col-lg-12 text-center col-xs-12 col-sm-12">

                <button type="submit" name="button" class="btn btn-default submit_kredit">Müraciət et</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- Start Preloader -->
<div id="loader">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<!-- End Preloader -->

<!-- Start Main Menu Area -->
<div id="home" class="mainmenu-area">
    <nav class="navbar navbar-expand-lg fixed-top menu-bg-cus" id="mainNav">
        <div class="container">
            <a class="navbar-brand" href="/"><img src="{{url('/front/img/logo.png')}}" alt="Logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto w-100 justify-content-end">
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger nav_size" href="#home">Ana Səhifə <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger nav_size" href="#about-us">Haqqımızda</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger nav_size" href="#services">Xidmətlərimiz</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger nav_size" href="#filial">Filiallarımız</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger nav_size" href="#contact">Bizimlə Əlaqə</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn btn-signup top_btn" href="tel:139"><img src="{{url('/front/img/call.svg')}}" alt="" style="width:22px;margin-right:10px;" class="phone">139</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<!-- End Start Main Menu Area -->


<!-- Start hero Area -->
<div class="hero-area" >
    <div class="d-table w-100 h-100">
        <div class="d-table-cell align-middle">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="slide-inner-content text-center">
                            <h1>PARANIZIN ETİBARLI EVİ!</h1>
                        {{--    @if(Session::has('success'))
                                  <h5 class="alert alert-success alert-dismissable col-md-12" style="color:#fff;">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                      {{ Session::get('success') }}
                                  </h5>
                              @endif
                            @if(Session::has('error'))
                                  <h3 class="alert alert-danger alert-dismissable col-md-12">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                      {{ Session::get('error') }}
                                  </h3>
                              @endif--}}
                            <div class="slide-btn">
                                <button  class="btn btn-common mr-10 gradientback open_kredit">Online kredit müraciəti</button>
                                <a href="#calculator"  class=" js-scroll-trigger link_kalk btn btn-common mr-10 gradientback link">Qızıl Kalkulyatoru</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="scroll-down">
        <a href="#about-us">
            <i class="icofont icofont-scroll-long-down"></i>
        </a>
    </div>
</div>
<!-- End Slides Area -->

<!-- Start About Us -->
<section id="about-us" class="about-us gray-bg ptb-80">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="about-img">
                    <img src="{{url('front/img/about.jpg')}}" class="img-thumbnail" alt="About Image" />
                </div>
            </div>

            <div class="col-lg-7">
                <div class="about-text">
                    <h2 class="heading">Haqqımızda</h2>
                    {!!$about->about!!}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End About Us -->

<!-- Start Our Services Area -->
<section id="services" class="our-services ptb-80">
    <div class="container">
        <div class="row">
          <div class="section-header col-md-12 ">
              <h2 class="text-center">Xidmətlərimiz</h2>
          </div>
          <!-- <div class="none_pad row"> -->
            @foreach($xidmet as $xid)
            <div class="col-md-6 col-sm-6 col-lg-6">
              <div class="content_service open_content" style="cursor:pointer;">
                <div class="service_icon">
                  <img src="/uploads/{{$xid->xidmet_icon}}" alt="" >

                </div>
                <div class="header_service">
                    <h4 class="text-center">{{$xid->xidmet_name}}</h4>
                </div>
                <div class="header_service content_text">
                      <div class="text">
                          {!!$xid->xidmet_text!!}
                      </div>
                </div>
              </div>
            </div>

@endforeach
  <div class="col-md-6 col-sm-6 col-lg-6">
    <div class="content_service open_content" style="cursor:pointer;">
      <div class="service_icon">
        <img src="{{url('front/img/chart.svg')}}" alt="" >

      </div>
      <div class="header_service">
          <h4 class="text-center">Struktur - maliyyə göstəriciləri</h4>
      </div>
      <div class="header_service content_text">
            <div class="text">
                {{-- <p><a href="{{url('front/pdf/struktur.PDF')}}" target="_blank">Struktur</a></p> --}}
                <p><a href="{{url('front/pdf/File0001.PDF')}}" target="_blank">Maliyyə hesabatları</a></p>
            </div>
      </div>
    </div>
  </div>
        <!-- </div> -->
    </div>
</section>


<section id="calculator" class=" ptb-80">

  <style media="screen">
      .main_calc_content{
          background-color: #0C56D9;
        border-radius: 5px;
      }
      .calc_content_main ,.calc_footer{
        background-color: #fff;
        padding:40px;
        border-radius: 5px;
      }
      .calc_footer{
        padding:15px 40px 10px 40px;
      }
      .calc_footer{
        background-color: #0C56D9;
      }
      .calc_content_main, .calc_footer, .credo_content_row, .credo_content_main{
        display: flex;
      }
      .credo_content_main{
        width: 80%;
        flex-wrap: wrap;
      }
      .sum_credit{
        width:20%;
      }
      .credo_content_row{
          width: 100%;
      }
      .credo_content{
          width: 33.33%;
          padding:10px 50px;
          display: flex;
          align-items: center;
          justify-content: center;
      }

      .credo_content p{
        color: #0C56D9;
        font-size: 14px;
        text-align: center;
        height: 35px;
        text-align: center;
        border: 1.2px solid #0C56D9;
        border-radius: 5px;
        display: flex;
        align-items: center;
        justify-content: center;
        width: 100%;
        color:#0C56D9;
        font-weight:bold;
      }
      .credo_content input{
        padding:0;
        border-radius: 5px;
        border:1.2px solid #0C56D9;
        height: 30px;
        text-align: center;
        background-color: transparent;
        width: 100%
      }
      .credo_content input:active,   .credo_content input:focus{
        outline: none !important;
      }
      .credo_content input::-webkit-outer-spin-button,
      .credo_content input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
      }
      .credo_content input::placeholder{
        color:#0C56D9
      }
        .credo_content input{
          color:#0C56D9;
        }
      /* Firefox */
      .credo_content input[type=number] {
        -moz-appearance: textfield;
      }
      .credo_content .price_cred{
        height: 35px;
        text-align: center;
        border: 1.2px solid #0C56D9;
        border-radius: 5px;
        display: flex;
        align-items: center;
        justify-content: center;
        width: 100%;
        color:#0C56D9
      }
      .credo_content .price_cred span{
        margin-right: 5px;
      }
      .sum_credit{
        display: flex;
        align-items: center;
        justify-content: center;
      }
      .sum_credit h3{
      text-align: center;
      width: 100%;
      font-weight: bold;
      font-size: 45px;
      color:#0C56D9;
      }
      .calc_footer .calc_footer_text{
      display: flex;
      width: 80%;
      }

      .calc_footer .footer_content{
        text-align:center;
        color:#fff;
        width:33.33%;
        padding:10px 50px;
        display: flex;
        align-items: center;
        justify-content: center;
      }
      .calc_footer .footer_content_last{
        width: 20%;
        text-align:center;
        color:#fff;
        position: relative;
      }
      .calc_footer .footer_content_last::before{
        position: absolute;
        top: -10%;
        left: 50%;
        content: '';
        width: 80%;
        height: 190%;
        background-color: #fff;
        border-radius: 5px;
        transform: translateX(-50%);
      }
        .calc_footer .footer_content_last span{
          position: relative;
          z-index: 10;
          color:#0C56D9;
          font-weight: 600;
          font-size: 14px;
        }
        @media (max-width:768px){
            .calc_content_main, .calc_footer{
              padding:10px 0;
            }
            .calc_footer .footer_content{
              padding:10px;
              font-size: 14px;
            }


            .credo_content {
              padding: 10px 5px;
            }
            .credo_content input{
              height: 35px;
            }
            .credo_content p{
              font-size: 12px;
            }
            .credo_content .price_cred{
              font-size: 11px;
            }

            .sum_credit h3 {
                font-size: 22px;
            }





            .calc_footer .footer_content_last span {

                font-size: 12px;
            }
            .calc_footer .footer_content_last::before {
              top: -5%;
              width: 90%;
          }
                          }
        @media (min-width:768px) and (max-width:992px){
          .calc_content_main, .calc_footer{
            padding:20px;
          }
          .calc_footer .footer_content_last span{
            font-size: 10px;
          }
        }
  </style>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjs/7.0.2/math.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
  <div class="container">
    <div class="main_calc_content">
      <div class="calc_footer">
        <div class="calc_footer_text">
          <div class="footer_content">
            ƏYAR
          </div>
          <div class="footer_content">
            QRAM
          </div>
          <div class="footer_content">
            KREDİT
          </div>
        </div>
        <div class="footer_content_last">
          <span>
            ÜMUMİ <br> KREDİT <br> MƏBLƏĞİ
          </span>
        </div>
      </div>
      <div class="calc_content_main">
        <div class="credo_content_main">
          <div class="credo_content_row">
            <div class="credo_content">
              <p>375</p>
            </div>
            <div class="credo_content">
              <input type="number" name="" value="" class="calc_input" placeholder="0" data-gold_weight="25.5" data-inputid="375">
            </div>
            <div class="credo_content">
              <div class="price_cred">
                <span id="sum375" class="price_kred">0 </span> AZN
              </div>
            </div>
          </div>
          <div class="credo_content_row">
            <div class="credo_content">
              <p>500</p>
            </div>
            <div class="credo_content">
              <input type="number" name="" value="" class="calc_input" placeholder="0" data-gold_weight="32.3" data-inputid="500">
            </div>
            <div class="credo_content">
              <div class="price_cred">
                <span id="sum500" class="price_kred">0 </span> AZN
              </div>
            </div>
          </div>
          <div class="credo_content_row">
            <div class="credo_content">
              <p>583</p>
            </div>
            <div class="credo_content">
              <input type="number" name="" value="" class="calc_input" placeholder="0" data-gold_weight="35.7" data-inputid="583">
            </div>
            <div class="credo_content">
              <div class="price_cred">
                <span id="sum583" class="price_kred">0 </span> AZN
              </div>
            </div>
          </div>
          <div class="credo_content_row">
            <div class="credo_content">
              <p>585</p>
            </div>
            <div class="credo_content">
              <input type="number" name="" value="" class="calc_input" placeholder="0" data-gold_weight="35.7" data-inputid="585">
            </div>
            <div class="credo_content">
              <div class="price_cred">
                <span id="sum585" class="price_kred">0 </span> AZN
              </div>
            </div>
          </div>
          <div class="credo_content_row">
            <div class="credo_content">
              <p>750 (ağlı)</p>
            </div>
            <div class="credo_content">
              <input type="number" name="" value="" class="calc_input" placeholder="0" data-gold_weight="40.8" data-inputid="7501">
            </div>
            <div class="credo_content">
              <div class="price_cred">
                <span id="sum7501" class="price_kred">0 </span> AZN
              </div>
            </div>
          </div>
          <div class="credo_content_row">
            <div class="credo_content">
              <p>750 </p>
            </div>
            <div class="credo_content">
              <input type="number" name="" value="" class="calc_input" placeholder="0" data-gold_weight="44.2" data-inputid="750">
            </div>
            <div class="credo_content">
              <div class="price_cred">
                <span id="sum750" class="price_kred">0 </span> AZN
              </div>
            </div>
          </div>
          <div class="credo_content_row">
            <div class="credo_content">
              <p>875 </p>
            </div>
            <div class="credo_content">
              <input type="number" name="" value="" class="calc_input" placeholder="0" data-gold_weight="47.6" data-inputid="875">
            </div>
            <div class="credo_content">
              <div class="price_cred">
                <span id="sum875" class="price_kred">0 </span> AZN
              </div>
            </div>
          </div>
          <div class="credo_content_row">
            <div class="credo_content">
              <p>916 </p>
            </div>
            <div class="credo_content">
              <input type="number" name="" value="" class="calc_input" placeholder="0" data-gold_weight="52.7" data-inputid="916">
            </div>
            <div class="credo_content">
              <div class="price_cred">
                <span id="sum916" class="price_kred">0 </span> AZN
              </div>
            </div>
          </div>
          <div class="credo_content_row">
            <div class="credo_content">
              <p>999</p>
            </div>
            <div class="credo_content">
              <input type="number" name="" value="" class="calc_input" placeholder="0" data-gold_weight="59.5" data-inputid="999">
            </div>
            <div class="credo_content">
              <div class="price_cred">
                <span id="sum999" class="price_kred"> 0 </span> AZN
              </div>
            </div>
          </div>
        </div>
        <div class="sum_credit">
          <h3 class="total_sum"><span>0</span> AZN</h3>
        </div>
      </div>


    </div>
  </div>
  <script type="text/javascript">

          $('.calc_input').on('keyup', function(event) {

                var gold_weight = $(this).val();
                var gold_quality = $(this).data('gold_weight');
                var id = $(this).data('inputid');
                var kredit_price = math.evaluate(gold_weight * gold_quality * 1.4);
                var print =  math.format(kredit_price, {precision: 14});

                $('#sum'+id).text(print);

               var prices = $('.price_kred');
               var price_element = numeral(0);

               prices.each(function(index, el) {
                  // total_price =  math.evaluate(total_price + parseInt(el.innerHTML))
                  element = parseFloat(el.innerHTML);
                  tota_price_sum = price_element.add(element);
               });
              $('.total_sum span').text(tota_price_sum.value());
          });
  </script>
</section>

<!-- End Our Services Area -->
<section id="filial" class="get-in-touch ptb-80">
  <div class="container">
      <div class="row">
        <div class="section-header col-md-12 ">
            <h2 class="text-center">Filiallarımız</h2>
        </div>

      </div>
  </div> 
<!-- Map Area -->
<div id="map"></div>
<!-- End Map Area -->
</section> 

<section id="contact" class="get-in-touch ptb-80">
    <div class="container">
        <div class="section-header text-center">
            <h2>Bizimlə Əlaqə</h2>
        </div>

        <div class="contact-form-area">

                <div class="row">
                    <div class="col-md-4 col-lg-4">
                        <div class="contact-info">
                            <ul>
                                <li>
                                    <h4>E-Poçt:</h4>
                                    <p>info@parabokt.az/p>
                                </li>
                                <li>
                                    <h4>Telefon:</h4>
                                    <p>(012) 447 10 00 <br> (012) 537 20 97</p>
                                </li>
                                <li>
                                    <h4>Ünvan:</h4>
                                    <p>Bakı şəh., Səməd Vurğun küç. 60</p>
                                </li>

                            </ul>
                        </div>
                    </div>

                    <div class="col-md-8 col-lg-8">
                        <form  method='post' action="/mail_send">
                          {{ csrf_field() }}
                      @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                {{ Session::get('success') }}
                            </div>
                        @endif
                      @if(Session::has('error'))
                            <div class="alert alert-danger alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                {{ Session::get('error') }}
                            </div>
                        @endif
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Ad" required data-error="Zəhmət olmasa adınızı qeyd edin">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="email" placeholder="E-Poçt" id="email" class="form-control" name="email" required data-error="Zəhmət olmasa e-poçtunuzu qeyd edin">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <textarea class="form-control" id="message" placeholder="İsmarıc" rows="8" data-error="Zəhmət olmasa ismarıcınızı qeyd edin" required name="message"></textarea>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="text-right">
                            <input type="submit" id="submit" class="btn btn-common gradientback" value="Göndər">
                            <div id="msgSubmit" class="msgsbmt hidden"></div>
                            <div class="clearfix"></div>
                        </div>
                      </form>

                    </div>
                </div>
        </div>
    </div>
</section>
<!-- Start Get in Touch Area -->



<!-- Start Footer Area -->
<footer class="footer footer-bg">
    <div class="container">
        <div class="footer-inner-content text-center">
            <div class="footer-logo navbar-brand">
                <a href="index-3.html"><img src="{{url('/front/img/logo.png')}}" alt="Logo"></a>
            </div>

            <div class="footer-menu">
                <ul>
                    <li><a href="#home" class="js-scroll-trigger">Ana Səhifə</a></li>
                    <li><a href="#about-us" class="js-scroll-trigger">Haqqımızda</a></li>
                    <li><a href="#services" class="js-scroll-trigger">Xidmətlərimiz</a></li>
                    <li><a href="#filial" class="js-scroll-trigger">Filiallarımız</a></li>
                    <li><a href="#contact" class="js-scroll-trigger">Bizimlə Əlaqə</a></li>
                </ul>
            </div>
            <div class="social-share-icons">
                <ul>
                    <li><a target="_blank" href="https://www.facebook.com/PARABOKT/" target="_blank"><i class="icofont icofont-social-facebook"></i></a></li>
                    <li><a target="_blank" href="https://twitter.com/parabokt" target="_blank"><i class="icofont icofont-social-twitter"></i></a></li>
                    <li><a target="_blank" href="https://www.instagram.com/parabokt/" target="_blank"><i class="icofont icofont-social-instagram"></i></a></li>
		    <li><a target="_blank" href="https://www.youtube.com/channel/UCd2_J5XPt127gW7Oxd14zmQ"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="footer-bottom text-center">
            <p>2018 © Bütün Hüquqlar Qorunur.</p>
        </div>
    </div>
</footer>
<!-- End Footer Area -->
<style media="screen">
   .swal-overlay--show-modal {
     display: block !important;
   }
   </style>
<!-- Back to top -->
<a class="scrolltop" href="#top"><i class="icofont icofont-thin-up"></i></a>

        <!-- jQuery -->

    <script src="{{url('front/js/popper.min.js')}}"></script>
    <script src="{{url('front/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3n3A_fDQnFoUS0oksGBLZtNspEfyS4ro"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{url('front/js/owl.carousel.min.js')}}"></script>
    <script src="{{url('front/js/jquery.easing.min.js')}}"></script>
    <script src="{{url('front/js/scrolling-nav.js')}}"></script>
    <script src="{{url('front/js/ekko-lightbox.min.js')}}"></script>
    <script src="{{url('front/js/wow.min.js')}}"></script>
    <script src="{{url('front/js/waypoints.min.js')}}"></script>
    <script src="{{url('front/js/jquery.counterup.min.js')}}"></script>
    <script src="{{url('front/js/form-validator.min.js')}}"></script>
    <script src="{{url('front/js/contact-form-script.js')}}"></script>
    <script src="{{url('front/js/maps.js')}}"></script>
    <script src="{{url('front/js/main.js')}}"></script>


        <!-- Map -->
        <!-- // <script src="js/map.js"></script> -->


  <script type="text/javascript">
$(function() {
    $("#map").goMap({
      hideByClick: true,
      scaleControl: true,
     maptype: 'ROADMAP' ,
     latitude: 56.948813,
      longitude: 24.104004,
      zoom: 7,
      navigationControl: true,
     mapTypeControl: false,
     scrollwheel: false,
      markers: [
        @foreach($filiallar as $filial)
        {
          latitude: {{$filial->filial_cordinat_x}},
          longitude: {{$filial->filial_cordinat_y}},
          html: {
              content: '<p><b style="color:black;">{{$filial->filial_name}}</b></p><p>{{$filial->filial_address}}<br>{{$filial->filial_number}}</p>',

          }
      },
    @endforeach
  ],

    });
});

</script> 
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '194349884731474');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=194349884731474&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
    </body>

    <script type="text/javascript">
    @if(Session::get('success_kred'))
        swal("Təşəkkür edirik!", "Müraciətiniz qəbul edildi!", "success");
      @endif
    </script>

</html>
