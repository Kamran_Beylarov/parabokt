<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','IndexController@index');
Route::post('/mail_send','IndexController@mailSend');
Route::post('/reuqest_kredo','IndexController@kredo_request');


Auth::routes();

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::resource('filiallar' ,'FilalController');
    Route::resource('services' ,'ServicesController');
    Route::resource('about' ,'AboutController');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
