<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Xidmet extends Model
{
  protected $table = 'xidmet';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
      'xidmet_name',
      'xidmet_icon',
      'xidmet_text',
  ];


}
