<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filial extends Model
{
  protected $table = 'filiallar';
protected $primaryKey = 'id';
public $timestamps = false;
protected $fillable = [

  'id','filial_name','filial_cordinat_x','filial_cordinat_y','filial_address','filial_number'
];


}
