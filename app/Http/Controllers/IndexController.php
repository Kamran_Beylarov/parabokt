<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Filial;
use App\About;
use App\Xidmet;
use Mail;
use Session;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
class IndexController extends Controller
{
    public function index(){
      $filiallar = Filial::all();
      $about = About::first();
      $xidmet= Xidmet::all();
      return view('index',compact('filiallar','about' ,'xidmet'));
    }

    public function mailSend(Request $request){


      $this->validate($request, [ 'name' => 'required|max:255', 'email' => 'required|email', 'message' => 'required' ]);

$mail = new PHPMailer(true);
try {
$mail->SMTPOptions = array(
 'ssl' => array(
     'verify_peer' => false,
     'verify_peer_name' => false,
     'allow_self_signed' => true
 )
);

$mail->SMTPDebug = 2;                                 // Enable verbose debug output
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'noreplay.parabokt@gmail.com';                 // SMTP username
$mail->Password = 'wgamqdcjykqdkwrn';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to
$mail->CharSet = 'UTF-8';
// dd($request);
//Recipients
$mail->setFrom('noreplay.parabokt@gmail.com', 'Parabokt');
$mail->addAddress('nigar.sultanova@parabokt.az', 'Parabokt');     // Add a recipient



//Content
$mail->isHTML(true);                                  // Set email format to HTML
$mail->Subject = 'Parabokt';
$mail->Body    = "<p><b>Ad</b>  :".$request['name']."</p><p> <b>E-Poçt</b> :".$request['email']."</p><p> <b>İsmarıc Mətni</b> :".$request['message']."</p>";

$mail->send();
Session::flash('success','İsmarıcınız Göndərildi');
return back();
} catch (Exception $e) {
Session::flash('error','İsmarıcınız Göndərilmədi');

return back();
}


   return redirect('/');
    }


public function kredo_request(Request $request){


    $this->validate($request, [ 'name_surname' => 'required|max:255',
                                'vesiqe_nom' => 'required',
                                'dogum_tarixi' => 'required',
                                 'unvan' => 'required',
                                 'yashayish_unvan' => 'required',
                                 'seher_nom' => 'required',
                                 'mob_nom' => 'required',
                                 'qizil_qram' => 'required',
                                  'qizil_eyyar' => 'required'
                                ]);

      $mail = new PHPMailer(true);
      try {
      $mail->SMTPOptions = array(
       'ssl' => array(
           'verify_peer' => false,
           'verify_peer_name' => false,
           'allow_self_signed' => true
       )
      );

      $mail->SMTPDebug = 2;                                 // Enable verbose debug output
      $mail->isSMTP();                                      // Set mailer to use SMTP
      $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
      $mail->SMTPAuth = true;                               // Enable SMTP authentication
      $mail->Username = 'noreplay.parabokt@gmail.com';                 // SMTP username
      $mail->Password = 'wgamqdcjykqdkwrn';                           // SMTP password
      $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
      $mail->Port = 587;
      $mail->CharSet = 'UTF-8';                                 // TCP port to connect to
      // dd($request);
      //Recipients
      $mail->setFrom('noreplay.parabokt@gmail.com', 'Parabokt online kredit müraciəti');
      // $mail->addAddress('aytac.shahveranova@parabokt.az', 'Parabokt online kredit müraciəti');     // Add a recipient
      // $mail->addAddress('nigar.sultanova@parabokt.az', 'Parabokt online kredit müraciəti');     // Add a recipient
      // $mail->addAddress('gulnar.aydali@parabokt.az', 'Parabokt online kredit müraciəti');     // Add a recipient
      // $mail->addAddress('xalida.heydarova@parabokt.az', 'Parabokt online kredit müraciəti');     // Add a recipient
      $mail->addAddress('onlinekredit@parabokt.az', 'Parabokt online kredit müraciəti');     // Add a recipient
      $mail->addAddress('onlinekredit@parabank.az', 'Parabokt online kredit müraciəti');     // Add a recipient
      $mail->addAddress('onlinesales@parabokt.az', 'Parabokt online kredit müraciəti');     // Add a recipient
      $mail->addAddress('onlinesales@parabank.az', 'Parabokt online kredit müraciəti');     // Add a recipient
      $mail->addAddress('kreditonline@parabokt.az', 'Parabokt online kredit müraciəti');     // Add a recipient
      $mail->addAddress('kreditonline@parabank.az', 'Parabokt online kredit müraciəti');     // Add a recipient
      // $mail->addAddress('kamran@endorphin.az', 'Parabokt online kredit müraciəti');     // Add a recipient
      // $mail->addAddress('beylarov747@outlook.com', 'Parabokt online kredit müraciəti');     // Add a recipient





      //Content
      $mail->isHTML(true);                                  // Set email format to HTML
      $mail->Subject = 'Parabokt online kredit müraciəti';
      $mail->Body    = "<p><b>Ad, Soyad, Ata adı</b>  :".$request['name_surname']."</p><p> <b>Şəxsiyyət vəsiqəsinin seriyası</b> :"
      .$request['vesiqe_nom']."</p><p> <b>Doğum tarixi</b> :".$request['dogum_tarixi']."</p><p> <b>Qeydiyyat ünvanı</b> :"
      .$request['unvan']."</p><p> <b>Yaşayış üvanı</b> :".$request['yashayish_unvan']."</p><p> <b>Şəhər nömrəsi</b> :"
      .$request['seher_nom']."</p><p> <b>Mobil nömrə</b> :".$request['mob_nom']."</p><p> <b>Qızıl-zinət əşyalarının qramı</b> :"
      .$request['qizil_qram']."</p><p> <b>Qızılın əyyarı</b> :".$request['qizil_eyyar'];

      $mail->send();
      Session::flash('success_kred','İsmarıcınız Göndərildi');
      return back();
      } catch (Exception $e) {
      Session::flash('error','İsmarıcınız Göndərilmədi');

      return back();
      }
  }
}
