<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Xidmet;
class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {

         $xidmetler = Xidmet::all();
         // dd($xidmetler);
         return view('admin.xidmet.index',compact('xidmetler'));
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {

       return view('admin.xidmet.create');
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {

       $img = $request->file('xidmet_icon');
       $icon_name = time().'.'.$img->getClientOriginalExtension();
       $img->move(public_path('uploads'), $icon_name);
       Xidmet::create([
         'xidmet_name'=>$request['xidmet_name'],
         'xidmet_text'=>$request['xidmet_text'],
         'xidmet_icon'=>$icon_name,

       ]);

       return redirect('/admin/services');
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
         //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
       // with('altXidmet')->
       $xidmet = Xidmet::find($id);

       return view('admin.xidmet.edit',compact('xidmet'));
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id)
     {

       $xidmet = Xidmet::find($id);

      if($request->file('xidmet_icon')){
             $img = $request->file('xidmet_icon');
             $icon_name = time().'.'.$img->getClientOriginalExtension();
             $img->move(public_path('uploads'), $icon_name);

             $xidmet->update([
               'xidmet_icon'=>$icon_name,
             ]);
      }
       $xidmet->update([
         'xidmet_name'=>$request['xidmet_name'],
         'xidmet_text'=>$request['xidmet_text'],
       ]);

       return redirect('/admin/services');
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
           $xidmet = Xidmet::find($id);

             $xidmet->delete();

             return redirect('/admin/services');
     }
}
