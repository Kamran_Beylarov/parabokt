<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Filial;
class FilalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filiallar = Filial::all();
        return view('admin.filiallar.index',compact('filiallar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.filiallar.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Filial::create([
          'filial_name'=> $request['filial_name'],
          'filial_cordinat_x'=> $request['filial_cordinat_x'],
          'filial_cordinat_y'=> $request['filial_cordinat_y'],
          'filial_address'=> $request['filial_address'],
          'filial_number'=> $request['filial_number']
        ]);
        return redirect('/admin/filiallar');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $filial = Filial::find($id);
          return view('admin.filiallar.edit',compact('filial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $filial = Filial::find($id);

          $filial->update([
            'filial_name'=> $request['filial_name'],
            'filial_cordinat_x'=> $request['filial_cordinat_x'],
            'filial_cordinat_y'=> $request['filial_cordinat_y'],
            'filial_address'=> $request['filial_address'],
            'filial_number'=> $request['filial_number']
          ]);
          return redirect('/admin/filiallar');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $filial = Filial::find($id);
      $filial->delete();
      return redirect('/admin/filiallar');
    }
}
